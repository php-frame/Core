<?php

if(!function_exists('env')) 
{
    function env($key, $default = null) {
        $value = getenv($key);

        if($value === false) {
            return $default;
        }

        return $value;
    }
}

if (!function_exists('app')) {
    function app()
    {
        return new class {
            public function version()
            {
                return '5.4';
            }
        };
    }
}

if (!function_exists('dd')) {
    function dd($var){
        var_dump($var);
        die();
    }
}

if (!function_exists('is_assoc')) {
    function is_assoc($var)
    {
        return is_array($var) && array_diff_key($var,array_keys(array_keys($var)));
    }
}