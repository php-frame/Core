<?php
namespace Frame;

function PhinxConfig($app){
    $app->bootstrap();

    $migrations = [];
    $seeds = [];

    $container = $app->getContainer();

    foreach($container->get('Modules') as $module){
        if($module::MIGRATIONS != null){
            $migrations[] = $module::MIGRATIONS;
        }
        if($module::SEEDS != null){
            $seeds[] = $module::SEEDS;
        }
    }

    return [
        'paths' => [
            'migrations' => $migrations,
            'seeds' => $seeds
        ],
        'environments' => [
            'default_database' => 'development',
            'development' => [
                'adapter' => $container->get('config')['database']['driver'],
                'host' => $container->get('config')['database']['host'],
                'port' => $container->get('config')['database']['port'],
                'user' => $container->get('config')['database']['username'],
                'pass' => $container->get('config')['database']['password'],
                'name' => $container->get('config')['database']['database'],
                'charset' => $container->get('config')['database']['charset']
            ]
        ]
    ];
}