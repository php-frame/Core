<?php
namespace Frame\Util;

class ViewData
{
    protected $data = [];

    public function merge($data = [], $target = [], $before = false){
        $mergedData = &$this->data;

        if(count($target) > 0){
            foreach($target as $key){
                $mergedData = &$mergedData[$key];
            }
        }

        if(is_array($data) && !is_assoc($data) && !$before){
            $mergedData = array_merge($mergedData ?? [], $data);
        }
        else{
            $mergedData = array_merge($data, $mergedData ?? []);
        }
    }

    public function getData(){
        return $this->data;
    }
}