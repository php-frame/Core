<?php
namespace Frame\Util;

use Carbon\Carbon;

class Cookie
{
    public static function exists($name)
    {
        return (isset($_COOKIE[$name])) ? true : false;
    }

    public static function get($name)
    {
        return $_COOKIE[$name];
    }

    public static function set($name, $value, $opt=[])
    {
        if(setcookie(
            $name, 
            $value, 
            $opt['expire'] ?? Carbon::now()->addWeek(2)->timestamp, 
            $opt['path'] ?? '/', 
            $opt['domain'] ?? null, 
            $opt['secure'] ?? null, 
            $opt['httponly'] ?? null)
        ) {
            return true;
        }

        return false;
    }

    public static function destroy($name)
    {
        self::set($name, '', time() - 1);
    }
}