<?php 
namespace Frame\Validation\Rules;

use Respect\Validation\Rules\AbstractRule;

class Unique extends AbstractRule
{
	protected $model;
	protected $column;

	public function __construct($model, $column){
		$this->model = $model;
		$this->column = $column;
	}

    public function validate($input)
    {
        return $this->model::where($this->column, $input)->first() == null;
    }
}