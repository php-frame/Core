<?php 
namespace Frame\Validation\Exceptions;

use \Respect\Validation\Exceptions\ValidationException;

class UniqueException extends ValidationException
{
    public static $defaultTemplates = [
        self::MODE_DEFAULT => [
            self::STANDARD => '{{name}} already in use',
        ],
        self::MODE_NEGATIVE => [
            self::STANDARD => '{{name}} not in use',
        ],
    ];
}