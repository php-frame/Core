<?php 
namespace Frame\Validation;

use Respect\Validation\Validator;

/**
 * Rule class
 */
class Rule{
	public function __construct($name){
		$this->name = $name;
		$this->validation = (new Validator)->setName($name);
	}

	public function __call($name, $args){
		return call_user_func_array([$this->validation, $name], $args);	
	}

	public function getName(){
		return $this->name;
	}
}