<?php

return [
    // Yeah, good luck
    // Below are languages dependant words/sentences

    'exception'=> [
        'upload' => [
            0 => 'There is no error, the file uploaded with success',
            1 => 'The uploaded file exceeds the upload_max_filesize directive in php.ini',
            2 => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form',
            3 => 'The uploaded file was only partially uploaded',
            4 => 'No file was uploaded',
            6 => 'Missing a temporary folder',
            7 => 'Failed to write file to disk.',
            8 => 'A PHP extension stopped the file upload.'
        ]
    ],
    'parts' => [
        'add' => 'Add',
        'create' => 'Create',
        'remove' => 'Remove',
        'delete' => 'Delete',
        'view' => 'View',
        'update' => 'Update',

        'added' => 'Added',
        'created' => 'Created',
        'removed' => 'Removed',
        'deleted' => 'Deleted',
        'updated' => 'Updated',
        'accepeted' => 'Accepted',
        'declined' => 'Declined',

        'not_exist' => 'Doesn\'t exist',

        'cancel' => 'Cancel',

        'the' => 'The',
        'this' => 'This',

        'success' => 'Success',
        'warning' => 'Warning',
        'fail' => 'Fail',
        'error' => 'Error',

        'accept' => 'Accept',
        'decline' => 'Decline',

        'successfully' => 'Successfully',

        'item' => [
            'single' => 'Item',
            'multiple' => 'Items'
        ],
        'step' => 'Step',
        'step_current' => 'Current step'
    ],
    'message' => [
        'check' => 'Are you sure ?',
        'feature_not_available' => 'Sorry, this feature is not available at the moment.',
        'error' => 'There was an error.'
    ],
    'form' => [
        'error' => 'Please fill out properly the form to continue.',
    ],
    'translation' => [

        // Production message only
        '404' => 'Translation not found ! :c'
    ],

    // This is generic messages, please updates them carefully (if needed to)
    'generic' => [
        'created' => '{{ label | default(translator.lang("parts.item.single")) }} {{ translator.lang("parts.successfully") | lower }} {{ translator.lang("parts.created") | lower }}',
        'deleted' => '{{ label | default(translator.lang("parts.item.single")) }} {{ translator.lang("parts.successfully") | lower }} {{ translator.lang("parts.deleted") | lower }}',
        'updated' => '{{ label | default(translator.lang("parts.item.single")) }} {{ translator.lang("parts.successfully") | lower }} {{ translator.lang("parts.updated") | lower }}',
        'accepted' => '{{ label | default(translator.lang("parts.item.single")) }} {{ translator.lang("parts.successfully") | lower }} {{ translator.lang("parts.accepeted") | lower }}',
        'declined' => '{{ label | default(translator.lang("parts.item.single")) }} {{ translator.lang("parts.successfully") | lower }} {{ translator.lang("parts.declined") | lower }}',
        '404' => '{{ translator.lang("parts.this") }} {{ label | default(translator.lang("parts.item.single")) | lower }} {{ translator.lang("parts.not_exist") | lower }}',
        
        'view' => '{{ translator.lang("parts.view") }} {{ translator.lang("parts.the") | lower }} {{ label | default(translator.lang("parts.item.single")) | lower }}',
        'create' => '{{ translator.lang("parts.create") }} {{ translator.lang("parts.the") | lower }} {{ label | default(translator.lang("parts.item.single")) | lower }}',
        'delete' => '{{ translator.lang("parts.delete") }} {{ translator.lang("parts.the") | lower }} {{ label | default(translator.lang("parts.item.single")) | lower }}',
        'update' => '{{ translator.lang("parts.update") }} {{ translator.lang("parts.the") | lower }} {{ label | default(translator.lang("parts.item.single")) | lower }}'
    ],
    'count' => [
        'default' => '{{ number }} {% if number == 1 %}{{ single | default(translator.lang("parts.item.single")) | raw }}{% else %}{{ multiple | default(translator.lang("parts.item.multiple")) | raw }}{% endif %}',
        'auto' => '{{ translator.lang("count.default", { number:number, single:translator.lang(ressource ~ ".single") | raw, multiple:translator.lang(ressource ~ ".multiple") | raw }) }}'
    ],
    'date' => [
        'ago' => '<span>{{ date | time_diff }} <i class="ico ico-information tool-tip"><span class="title">{{ date }}</span></i> </span>',
        'created' => '{{ translator.lang("parts.created") }} {{ translator.lang("date.ago", { date:date }) | raw }}'
    ]
];