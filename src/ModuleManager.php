<?php
namespace Frame;

/**
 * Frame module manager class
 * 
 * This class can manage modules and setting up them
 */
class ModuleManager
{
    /**
     * @var \Frame\Core $app The frame Core to inject into modules
     */
    private $app;

    /**
     * @var array[\Frame\Module]
     */
    private $modules;

    /**
     * Manager contructor, injecting frame Core in modules
     * 
     * @param \Frame\Core $app The frame Core
     */
    public function __construct(\Frame\Core $app){
        $this->app = $app;

        $this->modules = array_map(function($module){
            return new $module($this->app);
        }, $this->app->getContainer()->get('ClassModules'));
    }

    /**
     * Setting up each module
     * 
     * @return void
     */
    public function setup(){
        foreach($this->modules as $module){
            $module();
        }
    }

    /**
     * Getting all modules
     * 
     * @return array[\Frame\Module] The modules
     */
    public function getModules(){
        return $this->modules;
    }

    /**
     * Getting one module, by name
     * 
     * @param string $name The module name
     * @return \Frame\Module|null The founded module
     */
    public function getModule($name){
        foreach($this->modules as $module){
            if($module->getName() == $name){
                return $module;
            }
        }
        return null;
    }
}