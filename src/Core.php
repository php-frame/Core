<?php
namespace Frame;

use Slim\App;
use Slim\Container;
use Dotenv\Dotenv;
use Respect\Validation\Validator;
use Frame\Middleware\CsrfMiddleware;
use Frame\Middleware\OldInputMiddleware;
use Frame\Middleware\RememberMiddleware;
use Frame\Middleware\RouteMiddleware;
use Frame\Middleware\CoreMiddleware;
use Frame\Middleware\BrowserMiddleware;
use Frame\Middleware\OsMiddleware;

/**
 * Core frame application class
 */
class Core extends App
{
    /**
     * Constructor of the application, initializing ENV
     * 
     * @param array[class] $modules Modules class names
     */
    public function __construct($modules = [])
    {
        $this->pullEnv();

        parent::__construct(new \Slim\Container(array_merge([ 'ClassModules' => $modules ], require(__DIR__ . '/CoreContainer.php'))));

        set_error_handler([$this, "warningHandler"], E_WARNING);
    }

    /**
     * Pulling env variables
     * Setting up what need to be set up
     * 
     * @return void
     */
    private function pullEnv(){
        $root = null;
        $directory = dirname(dirname(dirname(__FILE__)));

        do {
            $composer = $directory . '/composer.json';
            if(file_exists($composer)) {
                $root = $directory;
                break;
            }
            $directory = dirname($directory);
        } while(is_null($root) && $directory != '/');

        if(is_null($root)){
            $root = dirname(dirname(__FILE__));
        }

        if(file_exists($root . '/.env')) {
            $env = new Dotenv($root);
            $env->load();
        }

        if(php_sapi_name() !== 'cli'){
            $app_url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]" . getenv("APP_URI");
            putenv("APP_URL=$app_url");
        }

        $app_path = realpath($root);
        putenv("APP_PATH=$app_path");
    }

    /**
     * Custom warning handler
     * 
     * @param \Error $e The catched error
     * @param string $str The error message
     * @return void
     */
    public function warningHandler($e, $str) { 
        $this->getContainer()['flash']->addMessage('warning', $str);
    }

    /**
     * Setting up modules and middewares
     * 
     * @return void
     */
    public function bootstrap(){
        $container = $this->getContainer();

        Validator::with('Frame\\Validation\\Rules');

        $container['ModuleManager'] = new \Frame\ModuleManager($this);
        $container['Modules'] = $container['ModuleManager']->getModules();

        $container['Database']->bootEloquent();

        if(php_sapi_name() !== 'cli' && env('APP_ENV') == 'development'){
            $whoopsGuard = new \Zeuxisoo\Whoops\Provider\Slim\WhoopsGuard();
            $whoopsGuard->setApp($this);
            $whoopsGuard->setRequest($container['request']);
            $whoopsGuard->setHandlers([]);
            $whoopsGuard->install();

            $this->add(new \Zeuxisoo\Whoops\Provider\Slim\WhoopsMiddleware($this));
        }

        // Setting up modules
        $container['ModuleManager']->setup();

        // Setting up global middlewares
        $this->add(CoreMiddleware::class);
        $this->add(RouteMiddleware::class);
        $this->add(RememberMiddleware::class);
        $this->add(CsrfMiddleware::class);
        $this->add($container->get('csrf'));
        $this->add(OldInputMiddleware::class);
        $this->add(BrowserMiddleware::class);
        $this->add(OsMiddleware::class);
        $this->add(new \RKA\Middleware\IpAddress(false, []));
    }

    /**
     * Runnig function
     * Providing bootstrap before parent running
     * 
     * @param bool|false $silent 
     * @return ResponseInterface
     */
    public function run($silent=false){
        $this->bootstrap();
        return parent::run($silent);
    }
}