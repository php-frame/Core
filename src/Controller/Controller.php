<?php
namespace Frame\Controller;

use Respect\Validation\Exceptions\ValidationException;

/**
 * All controllers should extend this class.
 */

class Controller
{
    protected $request, $response, $args, $viewData = [];

    use \Frame\Traits\ContainerAwareTrait;

    public function __invoke($request, $response, $args){
        $this->request = $request;
        $this->response = $response;
        $this->args = $args;

        $method = strtolower($this->request->getMethod());

        return $this->_handle($method);
    }

    public function __call($name, $args){
        $this->request = $args[0];
        $this->response = $args[1];
        $this->args = $args[2];

        $method = strtolower($this->request->getMethod());
        $method .= ucfirst($name);

        return $this->_handle($method);
    }

    public function _handle($method){
        if(method_exists($this, $method)){
            return $this->$method();
        }
        else{
            throw new \Exception("Method $method not found");
        }
    }

    public function param($name)
    {
        return $this->request->getParam($name);
    }

    public function flash($type, $message)
    {
        return $this->flash->addMessage($type, $message);
    }

    public function flashNow($type, $message)
    {
        return $this->flash->addMessageNow($type, $message);
    }

    public function render($name, array $args = [])
    {
        $args = array_merge($this->ViewData->getData(), $args);
        return $this->container->view->render($this->response, $name . '.twig', $args);
    }

    public function rawRedirect($path)
    {
        return $this->response->withRedirect($path, 301);
    }

    public function redirect($path = null, $args = [], $params = [])
    {
        if($path ?? false){
            return $this->rawRedirect($this->router->pathFor($path, $args, $params));
        }
        else{
            return $this->rawRedirect(env('APP_URI', '/'));
        }
    }

    public function back(){
        $current_uri = null;
        if($this->request->getAttribute('route') != null){
            $current_uri = $this->router->pathFor($this->request->getAttribute('route')->getName(), $this->request->getAttribute('route')->getArguments());
        }
        if($this->request->getAttribute('old_route') != null && $current_uri !== parse_url($this->request->getAttribute('old_route'))['path']){
            return $this->rawRedirect($this->request->getAttribute('old_route'));
        }
        else{
            return $this->redirect();
        }
    }

    public function validate($rules){
        $validated = true;
        $errors = [];

        foreach($rules as $rule){
            try{
                $rule->check($this->param($rule->getName()));
            }

            // Catch any error
            catch(ValidationException $e){
                if($validated){
                    $this->flashNow("error", $this->translator->lang('form.error'));
                    $validated = false;
                }
                $errors[$e->getName()] = $e->getMainMessage();
            }
        }

        if(!$validated){
            $this->ViewData->merge(['errors' => $errors]);
        }

        return $validated;
    }
}