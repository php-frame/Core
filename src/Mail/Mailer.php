<?php
namespace Frame\Mail;

use Frame\Mail\Message;

class Mailer
{
    protected $mailer;
    protected $container;

    public function __construct($mailer, $container)
    {
        $this->mailer = $mailer;
        $this->container = $container;
    }

    public function send($template, $data, $callback)
    {
        $message = new Message($this->mailer);

        $message->body($this->container->view->render($template, $data));

        call_user_func($callback, $message);

        $this->mailer->send();
    }
}