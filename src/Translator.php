<?php
namespace Frame;

class Translator 
{
	protected $container;
    protected $coreLang = null;

    public function __construct($container){
        $this->container = $container;
    }

    public static function supported_languages(){
        return array_map(function($file){
            return explode('.php', $file)[0];
        }, array_values(array_diff(scandir(__DIR__ . '/Lang'), array('..', '.'))));
    }

    public function generic($generic_id, $args=[]){
        $generic_id = 'generic.' . $generic_id;
        foreach($args as $key => $value){
            $args[$key] = $this->lang($value);
        }
        return $this->lang($generic_id, $args);
    }

    public function lang($msg_id, $args=[]){
        $tmp = explode('.', $msg_id);
        $messageContainer = [];
        $lang = env('LANG', self::supported_languages()[0]);
        $msg = '';
        if($msg_id != 'translation.404'){
            $msg = $this->lang('translation.404');
        }

        if(env('APP_ENV', 'development') == 'development'){
            $msg = ' {' . $msg_id . '}';
        }

        if(substr($msg_id, 0, 1) == "@"){
            $name = substr($tmp[0], 1);
            array_shift($tmp);

            $module = $this->container->get('ModuleManager')->getModule($name);
            $messageContainer = $module->loadLang($lang);
        }
        else{
            if($this->coreLang == null){
                if(file_exists(__DIR__ . '/Lang/' . $lang . '.php')){
                    $this->coreLang = require(__DIR__ . '/Lang/' . $lang . '.php');
                }
                else{
                    $this->coreLang = require(__DIR__ . '/Lang/en.php');
                }
            }
            $messageContainer = $this->coreLang;
        }

        $error = false;
        foreach($tmp as $key){
            if(array_key_exists($key, $messageContainer)){
                $messageContainer = $messageContainer[$key];
            }
            else{
                $error = true;
            }
        }

        if(!$error){
            $msg = $messageContainer;
        }

        return $this->container->view->fetchFromString($msg, $args);
    }
}