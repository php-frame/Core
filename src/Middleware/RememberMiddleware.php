<?php
namespace Frame\Middleware;

use Frame\Middleware\Middleware;
use Frame\Util\Cookie;
use Frame\Util\Session;

class RememberMiddleware extends Middleware
{
    public function __invoke($request, $response, $next)
    {
        if(Cookie::exists($this->dotGet('config.app.remember_id')) && !$this->auth->check()) {
            $data = Cookie::get($this->dotGet('config.app.remember_id'));
            $credentials = explode('.', $data);

            if(empty(trim($data)) || count($credentials) !== 2) {
                Cookie::destroy($this->dotGet('config.app.remember_id'));
                return $this->redirect($response, 'home');
            } else {
                $identifier = $credentials[0];
                $token = $this->hash->hash($credentials[1]);

                $user = $this->auth->where('remember_identifier', $identifier)->first();

                if($user) {
                    if($this->hash->verifyHash($token, $user->remember_token)) {
                        if($user->active) {
                            Session::set($this->dotGet('config.app.auth_id'), $user->id);
                            $response = $next($request, $response);
                            return $response;
                        } else {
                            Cookie::destroy($this->dotGet('config.app.remember_id'));
                            
                            $user->removeRememberCredentials();

                            $this->flash("warning", "Your account has not been activated.");
                            return $this->redirect($response, 'auth.login');
                        }
                    } else {
                        $user->removeRememberCredentials();
                    }
                }
            }
        }
        
        $response = $next($request, $response);
        return $response;
    }
}
