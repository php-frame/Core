<?php
namespace Frame\Middleware;

use Frame\Middleware\Middleware;

class FeatureNotAvailable extends Middleware
{
    public function __invoke($request, $response, $next)
    {
        $this->flash('warning', $this->translator->lang('message.feature_not_available'));
        return $this->back($request, $response);
    }
}
