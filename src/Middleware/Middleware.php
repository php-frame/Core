<?php
namespace Frame\Middleware;

use Interop\Container\ContainerInterface;

class Middleware
{
    use \Frame\Traits\ContainerAwareTrait;

    public function flash($type, $message)
    {
        $this->container->flash->addMessage($type, $message);
    }

    public function rawRedirect($response, $path)
    {
        return $response->withRedirect($path);
    }

    public function redirect($response, $path = null, $args = [], $params = [])
    {
        if($path ?? false){
            return $this->rawRedirect($response, $this->router->pathFor($path, $args, $params));
        }
        else{
            return $this->rawRedirect($response, env('APP_URI', '/'));
        }
    }

    public function back($request, $response){
        $current_uri = null;
        if($request->getAttribute('route') != null){
            $current_uri = $this->router->pathFor($request->getAttribute('route')->getName(), $request->getAttribute('route')->getArguments());
        }
        if($request->getAttribute('old_route') != null && $current_uri !== $request->getAttribute('old_route')){
            return $this->rawRedirect($response, $request->getAttribute('old_route'));
        }
        else{
            return $this->redirect($response);
        }
    }
}