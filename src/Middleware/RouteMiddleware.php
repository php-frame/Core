<?php
namespace Frame\Middleware;

use Frame\Middleware\Middleware;

class RouteMiddleware extends Middleware
{
    public function __invoke($request, $response, $next)
    {
        $route = $request->getAttribute('route');

        // return NotFound for non existent route
        if (empty($route)) {
            //throw new NotFoundException($request, $response);
            //$route = 'noname';
        }
        else{
            $this->container->view->getEnvironment()->addGlobal('route', $route);
        }

        $refererHeader = $request->getHeader('HTTP_REFERER');
        if ($refererHeader != null) {
            // Extract referer value
            $referer = array_shift($refererHeader);
            $request = $request->withAttribute('old_route', $referer);
        }

        $response = $next($request, $response);
        return $response;
    }
}
