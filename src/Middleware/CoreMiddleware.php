<?php
namespace Frame\Middleware;

use Frame\Middleware\Middleware;

class CoreMiddleware extends Middleware
{
    public function __invoke($request, $response, $next)
    {
        $langHeader = $request->getHeader('HTTP_ACCEPT_LANGUAGE');

        if ($langHeader) {
            // Extract value
            $value = array_shift($langHeader);
            $lang = substr($value, 0, 2);
            $request = $request->withAttribute('lang', $lang);
            putenv("LANG=$lang");
        }

        $response = $next($request, $response);
        return $response;
    }
}
