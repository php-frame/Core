<?php
namespace Frame\Middleware;

use Frame\Middleware\Middleware;

class BrowserMiddleware extends Middleware
{
    public function __invoke($request, $response, $next)
    {
        $browser = "Unknown Browser";

        if($request->getHeader('User-Agent')[0] ?? false){
            $user_agent = $request->getHeader('User-Agent')[0];

            $browser_array = [
                '/msie/i'       =>  'Internet Explorer',
                '/firefox/i'    =>  'Firefox',
                '/safari/i'     =>  'Safari',
                '/chrome/i'     =>  'Chrome',
                '/edge/i'       =>  'Edge',
                '/opera/i'      =>  'Opera',
                '/netscape/i'   =>  'Netscape',
                '/maxthon/i'    =>  'Maxthon',
                '/konqueror/i'  =>  'Konqueror',
                '/mobile/i'     =>  'Handheld Browser'
            ];

            foreach ($browser_array as $regex => $value) { 
                if (preg_match($regex, $user_agent)) {
                    $browser    =   $value;
                }
            }
        }

        $request = $request->withAttribute('browser', $browser);

        $response = $next($request, $response);
        return $response;
    }
}
