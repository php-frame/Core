<?php
namespace Frame\Middleware;

use Frame\Middleware\Middleware;

class OsMiddleware extends Middleware
{
    public function __invoke($request, $response, $next)
    {
        $os_platform = "Unknown OS Platform";
        
        if($request->getHeader('User-Agent')[0] ?? false){
            $user_agent = $request->getHeader('User-Agent')[0];

            $os_array = [
                '/windows nt 10/i'     =>  'Windows 10',
                '/windows nt 6.3/i'     =>  'Windows 8.1',
                '/windows nt 6.2/i'     =>  'Windows 8',
                '/windows nt 6.1/i'     =>  'Windows 7',
                '/windows nt 6.0/i'     =>  'Windows Vista',
                '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
                '/windows nt 5.1/i'     =>  'Windows XP',
                '/windows xp/i'         =>  'Windows XP',
                '/windows nt 5.0/i'     =>  'Windows 2000',
                '/windows me/i'         =>  'Windows ME',
                '/win98/i'              =>  'Windows 98',
                '/win95/i'              =>  'Windows 95',
                '/win16/i'              =>  'Windows 3.11',
                '/macintosh|mac os x/i' =>  'Mac OS X',
                '/mac_powerpc/i'        =>  'Mac OS 9',
                '/linux/i'              =>  'Linux',
                '/ubuntu/i'             =>  'Ubuntu',
                '/iphone/i'             =>  'iPhone',
                '/ipod/i'               =>  'iPod',
                '/ipad/i'               =>  'iPad',
                '/android/i'            =>  'Android',
                '/blackberry/i'         =>  'BlackBerry',
                '/webos/i'              =>  'Mobile'
            ];

            foreach ($os_array as $regex => $value) { 
                if (preg_match($regex, $user_agent)) {
                    $os_platform    =   $value;
                }
            }
        }

        $request = $request->withAttribute('operating_system', $os_platform);

        $response = $next($request, $response);
        return $response;
    }
}
