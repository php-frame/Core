<?php
namespace Frame\Traits;

trait ContainerAwareTrait{
	protected $container;
	
	public function __construct($container){
		$this->container = $container;
	}

	public function __get($property){
        if ($this->container->has($property)) {
            return $this->container->get($property);
        }
    }

    public function dotGet($dot_property){
    	$property_tree = explode('.', $dot_property);

    	$property = $this->container->get($property_tree[0]);
    	for($i = 1 ; $i < count($property_tree) ; $i++){
    		$property = $property[$property_tree[$i]];
    	}

    	return $property;
    }
}