<?php
return [
    'config' => [
        'app' => [
            'environment' => env('APP_ENV', 'development'),
            'url' => env('APP_URL', 'http://127.0.0.1'),
            'activation' => [
                'method' => env('APP_ACTIVATION_METHOD', 'mail'),
            ],
            'auth_id' => env('APP_AUTH_ID', 'user_id'),
            'remember_id' => env('APP_REMEMBER_ID', 'APP_REMEMBER_TOKEN'),
        ],
        'database' => [
            'driver' => env('DB_CONNECTION', 'mysql'),
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '3306'),
            'username' => env('DB_USERNAME', 'root'),
            'password' => env('DB_PASSWORD'),
            'database' =>  env('DB_DATABASE', 'auth'),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
        ],
        'mail' => [
            'host' => env('MAIL_HOST'),
            'port' => env('MAIL_PORT'),
            'username' => env('MAIL_USERNAME'),
            'password' => env('MAIL_PASSWORD'),
            'from.name' => env('MAIL_FROM_NAME', 'noreply'),
            'from' => env('MAIL_FROM', 'noreply@example.com'),
        ],
        'mongodb' => [
            'name'          => 'mongodb',
            'driver'        => 'mongodb',
            'host'          => env('MONGO_HOST', 'localhost'),
            'port'          => env('MONGO_PORT', 27017),
            'database'      => env('MONGO_DATABASE'),
            'username'      => env('MONGO_USERNAME'),
            'password'      => env('MONGO_PASSWORD'),
            'use_mongo_id'  => true,
        ]
    ],

    'settings' => [
        'debug' => getenv('APP_ENV') === "production" ? false : true,
        'whoops.editor' => 'sublime',
        'displayErrorDetails' => false,//getenv('APP_ENV') === "production" ? false : true,
        'determineRouteBeforeAppMiddleware' => true,
        'viewTemplatesDirectory' => __DIR__ . '/../ressources/views',
    ],
    'hash' => function($c) {
        return new \Frame\Util\Hash;
    },

    'flash' => function($c) {
        return new \Frame\Util\Flash;
    },

    'translator' => function($c){
        return new \Frame\Translator($c);
    },

    'view' => function($c) {
        $loader = [];

        $rootFolder = realpath(env('APP_PATH') . env('APP_TEMPLATES', '/templates'));
        if(file_exists($rootFolder) && scandir($rootFolder) != false){
            $scanned_directory = array_diff(scandir($rootFolder), array('..', '.'));

            foreach($scanned_directory as $folder){
                $loader[$folder] = [];
                $loader[$folder][] = realpath($rootFolder . '/' . $folder);
            }
        }

        foreach ($c->get('Modules') as $module) {
            if(!array_key_exists($module->getName(), $loader)){
                $loader[$module->getName()] = [];
            }
            
            if($module::VIEWS ?? false){
                $loader[$module->getName()][] = $module::VIEWS;
            }
        }

        $view = new \Slim\Views\Twig($loader, [
            'debug' => env('APP_ENV', 'development') === "production" ? false : true
        ]);

        $view->addExtension(new \Slim\Views\TwigExtension($c['router'], $c['request']->getUri()));
        $view->addExtension(new \Twig_Extension_Debug);
        $view->addExtension(new \Frame\CoreTwig($c));
        $view->addExtension(new \Twig_Extension_StringLoader());
        $view->addExtension(new \Twig_Extensions_Extension_Text());
        $view->addExtension(new \Twig_Extensions_Extension_Date());

        $view->getEnvironment()->addGlobal('flash', $c['flash']);
        $view->getEnvironment()->addGlobal('translator', $c['translator']);

        return $view;
    },

    'csrf' => function($c) {
        $guard = new \Slim\Csrf\Guard;

        $guard->setFailureCallable(function($request, $response, $next) use ($c) {
            $request = $request->withAttribute("csrf_status", false);
            if($request->getAttribute('csrf_status') === false) {
                $c['flash']->addMessage('error', "CSRF verification failed, terminating your request.");

                return $response->withRedirect(env('APP_URI', '/'));
            } else {
                return $next($request, $response);
            }
        });

        return $guard;
    },

    'Database' => function($c) {
        $capsule = new \Illuminate\Database\Capsule\Manager;

        $capsule->addConnection($c->get('config')['database'], 'default');

        return $capsule;
    },

    'Mail' => function($c) {
        $mailer = new \PHPMailer;

        /*$mailer->isSMTP();
        $mailer->Host = $c->get('config.mail.host');
        $mailer->Port = $c->get('config.mail.port');
        $mailer->Username = $c->get('config.mail.username');
        $mailer->Password = $c->get('config.mail.password');
        $mailer->SMTPAuth = true;
        $mailer->SMTPSecure = false;*/
        $mailer->FromName = $c->get('config')['mail']['from.name'];
        $mailer->From = $c->get('config')['mail']['from'];

        $mailer->isHTML(true);

        return new \Frame\Mail\Mailer($mailer, $c);
    },
    'ViewData' => function($c){
        $data = new \Frame\Util\ViewData;

        $c->view->getEnvironment()->addFunction(new Twig_Function('global', function () use ($data) {
            return $data->getData();
        }));


        return $data;
    }
];