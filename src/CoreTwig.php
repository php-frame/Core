<?php
namespace Frame;

class CoreTwig extends \Twig_Extension
{
    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function getName()
    {
        return 'Core';
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('asset', [$this, 'asset']),
            new \Twig_SimpleFunction('getenv', [$this, 'getenv']),
            new \Twig_SimpleFunction('path_exist', [$this, 'path_exist'])
        ];
    }

    public function asset($name)
    {
        return env('APP_URL') . '/' . $name;
    }

    public function getenv($key, $default = null)
    {
        return env($key, $default);
    }

    public function path_exist($path){
        $routes = $this->container->router->getRoutes();

        foreach($routes as $route){
            if($route->getName() == $path){
                return true;
            }
        }
        return false;
    }
}
