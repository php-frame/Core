<?php
namespace Frame;

class Module 
{
	protected $app;
    protected $name;
    protected $prefix;
    protected $directory;
    protected $routes = [];
    protected $required = [];
    protected $supported_languages = [];
    protected $cacheLang = null;
    private $rootModuleName = 'Module';

    const MIGRATIONS = null;
    const SEEDS = null;
    const VIEWS = null;
    const LANGUAGES = null;

    public function __construct($app){
        $this->app = $app;
        $this->name = (new \ReflectionClass($this))->getShortName();
        $this->prefix = '@' . $this->name;
        $this->directory = dirname((new \ReflectionClass(get_class($this)))->getFileName());

        $languages = constant(get_class($this) . '::LANGUAGES');

        if(file_exists($languages)){
            $this->supported_languages = array_map(function($file){
                return explode('.php', $file)[0];
            }, array_values(array_diff(scandir($languages), array('..', '.'))));
        }

        $this->routes['settings'] = $this->routeName('settings');
    }

    public function __invoke(){
        // TODO
    }

    public function loadLang($lang){
        if(!in_array($lang, $this->supported_languages)){
            $lang = $this->supported_languages[0] ?? null;
        }

        $languages = constant(get_class($this) . '::LANGUAGES');
        $languageFile = $languages . '/' . $lang . '.php';

        if($this->cacheLang == null){
            if(file_exists($languageFile)){
                $this->cacheLang = require($languageFile);
            }
            else{
                $this->cacheLang = [];
            }
        }
        return $this->cacheLang;
    }

    public function routeName($name){
        return $this->prefix . '.' . $name;
    }

    public function getName() { return $this->name; }
    public function setName($name) { $this->name = $name; }
    public function getRoutes(){ return $this->routes; }
    public function getRequired(){ return $this->required; }
    public function getPrefix(){ return $this->prefix; }
    public function getDirectory(){ return $this->directory; }
}