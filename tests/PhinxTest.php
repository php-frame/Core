<?php
use PHPUnit\Framework\TestCase;

class PhinxTest extends TestCase
{
	protected $app;

	protected function setUp(){
		$this->app = new \Frame\Core;
	}

    public function testSetup()
    {
        $config = \Frame\PhinxConfig($this->app);
        $this->assertNotEmpty($config);
    }
}
?>