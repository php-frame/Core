<?php
use PHPUnit\Framework\TestCase;

use Slim\Http\Request;

class TestModule extends \Frame\Module{
    public function __invoke(){
        $this->app->get('/test', function($req, $res){
            return $res->getBody()->write('Test');
        });
    }
}

class AppTest extends TestCase
{	
	protected $app;

	protected function setUp()
    {
        $this->app = new \Frame\Core;
        $this->app->any('/', function($request, $response, $args){
            return $response;
        });
    }

    public function testRootFolder(){
    	$root = realpath(__DIR__ . '/../');
    	$this->assertEquals($this->app->getRootFolder(), $root);
    	$this->assertEquals(env('APP_PATH'), $root);
    }

    public function testAddModule(){
    	$this->app = new \Frame\Core([TestModule::class]);
        $this->app->bootstrap();

    	$this->assertEquals(count($this->app->getContainer()->get('Modules')), 1);
    }

    public function testCoreCallStack(){
        $env = \Slim\Http\Environment::mock([
            'REQUEST_URI' => '/'
        ]);

        $this->app->getContainer()['request'] = Request::createFromEnvironment($env);

        $this->app->run();

        $this->assertNotEmpty($this->app);
    }

    public function testCoreCallStackWithModule(){
        $this->app = new \Frame\Core([TestModule::class]);

        $env = \Slim\Http\Environment::mock([
            'REQUEST_URI' => '/test'
        ]);

        $this->app->getContainer()['request'] = Request::createFromEnvironment($env);

        $this->app->run();

        $this->assertNotEmpty($this->app);
    }
}
?>